import logo from "./logo.svg";
import firebase from "./firebase";
import "./App.css";
import axios from "axios";
import { useState } from "react";
function App() {
  const [qrImage, setQrImage] = useState();
  const [isPaid, setIsPaid] = useState(false);
  //immulator ajilluulj bolno, bnbn deploy hiih shaardlagagu
  const onClickButton = async () => {
    const response = await axios.post(
      "https://us-central1-qr-payment-cff6d.cloudfunctions.net/shop_create_invoice",
      {
        price: 5000, //form-oosoo avna tsaanaa avahgu zuger damjulj bga
        description: "klklk",
        name: "kdkdkd"
      }
    );
    const { qr, invoiceId } = response.data;
    setQrImage(qr);
    firebase
      .firestore()
      .doc(`invoices/${invoiceId}`)
      .onSnapshot(doc => {
        console.log("onsnapshot called");
        console.log(doc);
      });

    console.log(qrImage);
  };
  return (
    <div className="App">
      <header className="App-header">
        <img src={qrImage ? qrImage : logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <button onClick={onClickButton}>click me!</button>
      </header>
    </div>
  );
}

export default App;
