import firebase from "firebase/app";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyC5OJ8vMEUcRtP-pxV11L36wg7G9O5dOmw",
  authDomain: "qr-payment-cff6d.firebaseapp.com",
  projectId: "qr-payment-cff6d",
  storageBucket: "qr-payment-cff6d.appspot.com",
  messagingSenderId: "507649632561",
  appId: "1:507649632561:web:7c5a0748cacff667122810"
};

firebase.initializeApp(firebaseConfig);

export const firestore = firebase.firestore();
export default firebase;
