import "./App.css";
import logo from "./logo.svg";
import firebase from "./firebase";
import axios from "axios";
import { useState } from "react";

function App() {
  const [name, setName] = useState();
  const [qrImage, setQrImage] = useState();
  const [isPaid, setIsPaid] = useState(false);

  const handleSubmit = async e => {
    e.preventDefault();
    alert(`Submitting Name: ${name}`);

    const response = await axios.post(
      "https://us-central1-qr-payment-cff6d.cloudfunctions.net/shop_create_invoice",
      {
        name: { name },
        price: 15
      }
    );
    const { qr, invoiceId } = response.data;
    setQrImage(qr);
    firebase
      .firestore()
      .doc(`invoice/${invoiceId}`)
      .onSnapshot(doc => {
        console.log("onsnapshot called");
        console.log(doc);
      });
    console.log(qrImage);
  };

  return (
    <div>
      <header className="home center flex-row">
        <img src={qrImage ? qrImage : logo} className="App-logo" alt="logo" />
        <form className="flex-column" type onSubmit={handleSubmit}>
          <label>
            Name:
            <input
              type="text"
              name="name"
              value={name}
              onChange={e => setName(e.target.value)}
            />
          </label>
          <input type="submit" value="Submit" />
        </form>
      </header>
    </div>
  );
}

export default App;
