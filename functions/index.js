// const functions = require("firebase-functions");
// const cors = require("cors")({ origin: true });
// const axios = require("axios");
// const qrcode = require("qrcode");
// const admin = require("firebase-admin");
// admin.initializeApp();

// const baseUrl =
//   "https://us-central1-qr-payment-cff6d.cloudfunctions.net/somefunction";
// exports.shop_create_invoice = functions.https.onRequest((request, response) => {
//   cors(request, responce, async () => {
//     const db = admin.firestore();
//     const invoice = await db.collection("invoices").add({
//       name: "some name"
//     });
//     qrcode.toDataURL(`${baseUrl}?invoiceId=${invoice.id}`, (err, code) => {
//       if (err) return console.log("error occured");

//       response.send({
//         qr: code,
//         invoiceId: invoice.id
//       });
//     });
//     functions.logger.log("sending response to client");
//     response.send({
//       invoiceId: invoice.id
//     });
//   });
// });

// exports.somefunction = functions.https.onRequest(async (request, response) => {
//   const { invoiceId } = resuest.body;
//   await db.doc(`invoice/${invoiceId}`).set(
//     {
//       status: "paid"
//     },
//     {
//       merge: true
//     }
//   );
//   response.send({
//     status: "success"
//   });
// });

const functions = require("firebase-functions");
const admin = require("firebase-admin");
const axios = require("axios");
const qrcode = require("qrcode");
const cors = require("cors")({ origin: true });
const baseUrl =
  "https://us-central1-qr-payment-cff6d.cloudfunctions.net/somefunction";
// "https://us-central1-qr-payment-13a96.cloudfunctions.net/somefunction";

admin.initializeApp();

//Set-ExecutionPolicy -ExecutionPolicy Bypass -Scope CurrentUser
exports.shop_create_invoice = functions.https.onRequest((request, response) => {
  cors(request, response, async () => {
    console.log("aaaa", request, response);
    const db = admin.firestore();
    functions.logger.log(request.body);
    functions.logger.log("aaaa");

    const invoice = await db.collection("invoices").add({});

    // bank qr generate
    qrcode.toDataURL(`${baseUrl}?invoiceId=${invoice.id}`, (err, code) => {
      if (err) return console.log("error occurred");

      response.send({
        qr: code,
        invoiceId: invoice.id // base64 bolgoj bga code
      });
    });
  });
});

exports.somefunction = functions.https.onRequest(async (request, response) => {
  const db = admin.firestore();
  const { invoiceId } = request.query;
  // functions.logger.log("invoiceId", invoiceId);
  await db.doc(`invoices/${invoiceId}`).set(
    {
      status: "paid"
    },
    {
      merge: true
    }
  );
  response.send({
    status: "success"
    // qr unshuulanguut linkruu amjilttai orsoniig utasnii delgetsen dr message haruulna.
  });
});
