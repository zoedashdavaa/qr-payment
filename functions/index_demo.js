const functions = require("firebase-functions");
const cors = require("cors")({ origin: true });
const axios = require("axios");
const qrcode = require("qrcode");
const admin = require("firebase-admin");
admin.initializeApp();

const baseUrl =
  "https://us-central1-qr-payment-cff6d.cloudfunctions.net/somefunction";
exports.shop_create_invoice = functions.https.onRequest((request, response) => {
  cors(request, responce, async () => {
    const db = admin.firestore();
    const invoice = await db.collection("invoices").add({
      name: "some name"
    });
    qrcode.toDataURL(`${baseUrl}?invoiceId=${invoice.id}`, (err, code) => {
      if (err) return console.log("error occured");

      response.send({
        qr: code,
        invoiceId: invoice.id
      });
    });
    functions.logger.log("sending response to client");
    response.send({
      invoiceId: invoice.id
    });
  });
});

exports.somefunction = functions.https.onRequest(async (request, response) => {
  const { invoiceId } = resuest.body;
  await db.doc(`invoice/${invoiceId}`).set(
    {
      status: "paid"
    },
    {
      merge: true
    }
  );
  response.send({
    status: "success"
  });
});
